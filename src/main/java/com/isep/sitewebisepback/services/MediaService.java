package com.isep.sitewebisepback.services;

import com.isep.sitewebisepback.model.Media;

import java.util.List;

public interface MediaService {
    Media getMediaById(Long id);
    Media saveMedia(Media media);
    Media updateMedia(Media media);
    List<Media> createMedia(List<Media> medias);
    void deleteMedia(Long id);
    List<Media> getAllMedias();

}
