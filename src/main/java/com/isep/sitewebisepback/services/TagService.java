package com.isep.sitewebisepback.services;

import com.isep.sitewebisepback.model.Tag;

import java.util.List;

public interface TagService {
    Tag getTagById(Long id);
    Tag  saveTag(Tag tag);
    Tag updateTag (Tag tag);
    void deleteTag (Long id);
    List<Tag> getAllTags();
}
