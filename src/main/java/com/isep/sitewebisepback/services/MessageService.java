package com.isep.sitewebisepback.services;

import com.isep.sitewebisepback.model.Media;
import com.isep.sitewebisepback.model.Message;

import java.util.List;

public interface MessageService {
    Message getMessageById(Long id);
    Message saveMessage(Message message);
    Message updateMessage(Message message);
    void deleteMessage(Long id);
    List<Message> getAllMessage();
    List<Message> getMessagesSent();
    List<Message> getMessagesReceived();
    List<Message> getMessagesLu();
    List<Message> getMessagesNonLu();
}
