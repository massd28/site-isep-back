package com.isep.sitewebisepback.services;

import com.isep.sitewebisepback.model.Departement;
import com.isep.sitewebisepback.model.Filiere;

import java.util.List;

public interface FiliereService {
    Filiere getFiliereById(Long id);
    Filiere saveFiliere(Filiere filiere);
    Filiere updateFiliere(Filiere filiere);
    void deleteFiliere(Long id);
    List<Filiere> getAllFilieres();
    List<Filiere> getFilieresByDepartement(Long id);
}
