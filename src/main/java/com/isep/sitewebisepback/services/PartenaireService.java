package com.isep.sitewebisepback.services;

import com.isep.sitewebisepback.model.Partenaire;

import java.util.List;

public interface PartenaireService {
    Partenaire getPartenaireById(Long id);
    Partenaire savePartenaire(Partenaire partenaire);
    Partenaire updatePartenaire(Partenaire partenaire);
    void deletePartenaire(Long id);
    List<Partenaire> getAllPartenaires();
}
