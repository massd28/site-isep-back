package com.isep.sitewebisepback.services;
import com.isep.sitewebisepback.model.*;


import java.time.LocalDateTime;
import java.util.List;

public interface ArticleService {
    Article getArticleById(Long id);
    Article saveArticle(Article article);
    Article updateArticle (Article article);
    void deleteArticle (Long id);
    List<Article> getAllArticles();
    List<Article> getNews();
    //List<Article> getArticleByMediaId(Long id);
    List<Article> getArticleByTagName(String nom);
    List<Media> getMediasByArticle (Long id);
    List<Article> getArticlesByPage(String page);
    List<Article> getArticlesPublies();
    void deleteMedia (Long  idMedia, Long idArticle );
    List<Article> getArticlesByPartenaire(Long idPart);
    List<Article> getArticlesEcoPublies(String page);
}