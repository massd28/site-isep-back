package com.isep.sitewebisepback.services;

import com.isep.sitewebisepback.model.Label;

import java.util.List;

public interface LabelService {
    Label getLabelById(Long id);
    Label saveLabel(Label label);
    Label updateLabel(Label label);
    void deleteLabel(Long id);
    List<Label> getAllLabel();
}
