package com.isep.sitewebisepback.services;

import com.isep.sitewebisepback.model.User;

import java.util.List;

public interface UserService  {
    User getUserById(Long id);
    User saveUser(User user);
    User updateUser(User user);
    void deleteUser(Long id);
    List<User> getAllUsers();
    User login(String username, String password, String role);
}
