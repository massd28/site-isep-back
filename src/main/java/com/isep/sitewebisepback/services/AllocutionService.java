package com.isep.sitewebisepback.services;

import com.isep.sitewebisepback.model.Allocution;

import java.util.List;

public interface AllocutionService {
    Allocution getAllocutionById(Long id);
    Allocution updateAllocution(Allocution allocution);
    Allocution saveAllocution(Allocution allocution);
    void deleteAllocution(Long id);
    List<Allocution> getAllAllocutions();
    Allocution getRandomAllocution();

}
