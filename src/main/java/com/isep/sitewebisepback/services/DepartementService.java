package com.isep.sitewebisepback.services;

import com.isep.sitewebisepback.model.Departement;
import com.isep.sitewebisepback.model.Filiere;
import com.isep.sitewebisepback.model.Media;

import java.util.List;

public interface DepartementService {
    Departement getDepartementById(Long id);
    Departement saveDepartement(Departement departement);
    Departement updateDepartement(Departement departement);
    void deleteDepartement(Long id);
    List<Departement> getAllDepartements();
    List<Filiere> getFilieresByDepartement(Long idDepartement);
    List<Media> getMediasByDepartement(Long idDepartement);
}
