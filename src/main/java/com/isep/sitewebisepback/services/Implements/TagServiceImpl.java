package com.isep.sitewebisepback.services.Implements;

import com.isep.sitewebisepback.exceptions.BadRequestException;
import com.isep.sitewebisepback.exceptions.ResourceNotFoundException;
import com.isep.sitewebisepback.model.Article;
import com.isep.sitewebisepback.model.Tag;
import com.isep.sitewebisepback.model.User;
import com.isep.sitewebisepback.repository.TagRepository;
import com.isep.sitewebisepback.services.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class TagServiceImpl implements TagService {

    @Autowired
    private TagRepository tagRepository;

    @Override
    public Tag getTagById(Long id) {
        Tag tag = tagRepository.getOne(id);
        if (tag == null) {
            throw new ResourceNotFoundException("l'id n'est pas bon");
        }
        return tagRepository.getOne(id);
    }

    @Override
    public Tag saveTag(Tag tag) {
        return tagRepository.save(tag);
    }

    @Override
    public Tag updateTag(Tag tag) {
        return tagRepository.saveAndFlush(tag);
    }

    @Override
    public void deleteTag(Long id) {
        Tag tag = tagRepository.getOne(id);
        if (tag == null) {
            throw new ResourceNotFoundException("l'id n'est pas bon");
        }
        tagRepository.deleteById(id);
    }

    @Override
    public List<Tag> getAllTags() {
        return tagRepository.findAll();

    }
}

