package com.isep.sitewebisepback.services.Implements;

import com.isep.sitewebisepback.exceptions.BadRequestException;
import com.isep.sitewebisepback.exceptions.ResourceNotFoundException;
import com.isep.sitewebisepback.model.Article;
import com.isep.sitewebisepback.model.Departement;
import com.isep.sitewebisepback.model.Media;
import com.isep.sitewebisepback.model.Filiere;
import com.isep.sitewebisepback.repository.DepartementRepository;
import com.isep.sitewebisepback.repository.MediaRepository;
import com.isep.sitewebisepback.services.DepartementService;
import com.isep.sitewebisepback.utils.UtilBase64Image;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
@Service
@Transactional
public class DepartementServiceImpl implements DepartementService {
    @Autowired
    private DepartementRepository departementRepository;
    @Autowired
    private MediaRepository mediatRepository;
   // @Value("${pathFile}")
  //  private String pathFile;
    @Override
    public Departement getDepartementById(Long id) {
        Departement  departement = departementRepository.getOne(id);
        if (departement == null){
            throw new ResourceNotFoundException("l'id n'est pas bon");
        }
        return departement;
    }

    @Override
    public Departement saveDepartement(Departement departement) {
       List<Media> medias = new ArrayList<> (departement.getMedias());
        departement.setMedias(new ArrayList<>());
        Departement departement1 = departementRepository.save(departement);
        for (Media media : medias){
            System.out.println(media.getId() + "@@@@ID MEDIA");
            Media m = mediatRepository.getOne(media.getId());
            if(m != null){
                m.setDepartement(departement1);
                mediatRepository.saveAndFlush(m);
            }
        }

        return departement1;
    }

    @Override
    public Departement updateDepartement(Departement departement) {
       Departement departement1 = departementRepository.getOne(departement.getId());
       if (departement1 == null){
           throw new ResourceNotFoundException("dept introuvable");
       }
       List<Media> medias = new ArrayList<>(departement.getMedias());
       departement.setMedias(new ArrayList<>());
       for (Media media: medias){
           Media m = mediatRepository.getOne(media.getId());
           if(m != null){
               m.setDepartement(departement);
               mediatRepository.saveAndFlush(m);
           }
       }
        return departementRepository.saveAndFlush(departement);
    }

    @Override
    public void deleteDepartement(Long id) {
        Departement  departement = departementRepository.getOne(id);
        if (departement == null){
            throw new ResourceNotFoundException("l'id n'est pas bon");
        }
        departementRepository.deleteById(id);
    }

    @Override
    public List<Departement> getAllDepartements() {
        return departementRepository.findAll();
    }

    @Override
    public List<Filiere> getFilieresByDepartement(Long idDepartement) {
        Departement departement= departementRepository.getOne(idDepartement);
        return departement.getFilieres();
    }
    @Override
    public List<Media> getMediasByDepartement(Long idDepartement) {
        Departement departement= departementRepository.getOne(idDepartement);
        return departement.getMedias();
    }
}
