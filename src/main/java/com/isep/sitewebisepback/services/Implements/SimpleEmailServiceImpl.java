package com.isep.sitewebisepback.services.Implements;
import com.isep.sitewebisepback.exceptions.BadRequestException;
import com.isep.sitewebisepback.exceptions.ResourceNotFoundException;
import com.isep.sitewebisepback.model.Article;
import com.isep.sitewebisepback.model.Message;
import com.isep.sitewebisepback.model.Tag;
import com.isep.sitewebisepback.model.User;
import javax.mail.internet.MimeMessage;
import com.isep.sitewebisepback.repository.TagRepository;
import com.isep.sitewebisepback.services.SimpleEmailService;
import com.isep.sitewebisepback.services.TagService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.mail.internet.MimeMessage;
import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional

public class SimpleEmailServiceImpl implements SimpleEmailService {
    @Autowired
    private JavaMailSender sender;
    private Message mail;

    @Override
    public void sendEmail(Message mail) throws Exception{


        MimeMessage message = sender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        helper.setTo(mail.getEmailDestinataire());
        helper.setText(mail.getCorps());
        helper.setSubject(mail.getObjet());

        sender.send(message);
    }
}
