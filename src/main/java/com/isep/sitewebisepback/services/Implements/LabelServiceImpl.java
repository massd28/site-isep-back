package com.isep.sitewebisepback.services.Implements;

import com.isep.sitewebisepback.exceptions.ResourceNotFoundException;
import com.isep.sitewebisepback.model.Article;
import com.isep.sitewebisepback.model.Label;
import com.isep.sitewebisepback.model.Media;
import com.isep.sitewebisepback.model.Message;
import com.isep.sitewebisepback.repository.LabelRepository;
import com.isep.sitewebisepback.repository.MediaRepository;
import com.isep.sitewebisepback.repository.MessageRepository;
import com.isep.sitewebisepback.services.LabelService;
import com.isep.sitewebisepback.services.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;


@Service
@Transactional
public class LabelServiceImpl implements LabelService {

    @Autowired
    private LabelRepository labelRepository;

    @Override
    public Label getLabelById(Long id) {
        Label label = labelRepository.getOne(id);
        if (label == null) {
            throw new ResourceNotFoundException("l'id n'est pas bon");
        }
        return labelRepository.getOne(id);
    }

    @Override
    public Label saveLabel(Label label) {

        return labelRepository.save(label);
    }

    @Override
    public Label updateLabel(Label label) {
        return labelRepository.saveAndFlush(label);
    }

    @Override
    public void deleteLabel(Long id) {
        Label label = labelRepository.getOne(id);
        if (label == null) {
            throw new ResourceNotFoundException("l'id n'est pas bon");
        }
        labelRepository.deleteById(id);
    }

    @Override
    public List<Label> getAllLabel() {
        List<Label> inverse;
        inverse = labelRepository.findAll();
        Collections.reverse(inverse);
        return inverse;
    }


}
