package com.isep.sitewebisepback.services.Implements;

import com.isep.sitewebisepback.exceptions.BadRequestException;
import com.isep.sitewebisepback.exceptions.ResourceNotFoundException;
import com.isep.sitewebisepback.model.*;
import com.isep.sitewebisepback.repository.*;
import com.isep.sitewebisepback.services.EventsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import javax.transaction.Transactional;
import java.io.File;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class EventsServiceImpl implements EventsService {
    @Autowired
    private EventsRepository eventsRepository;
    @Autowired
    private MediaRepository mediaRepository;
    @Autowired
    private TagRepository tagRepository;
    @Autowired
    private MediaArticleRepository mediaArticleRepository;
    @Autowired
    private ArticleTagRepository articleTagRepository;

    @Override
    public Events getEventsById(Long id) {
        Events  events = eventsRepository.getOne(id);
        if (events == null){
            throw new ResourceNotFoundException("l'id n'est pas bon");
        }
        return events;
    }

    @Override
    public Events saveEvents(Events events) {
        List<MediaArticle> mediaArticles = new ArrayList<>(events.getMediaArticles());
        List<ArticleTag> articleTags = new ArrayList<>(events.getArticleTags());
        events.setMediaArticles(new ArrayList<>());
        events.setArticleTags(new ArrayList<>());
        events.setDateCreation(LocalDateTime.now());
        Events e = eventsRepository.save(events);
        for(MediaArticle mediaArticle: mediaArticles){
            mediaArticle.getPk().setArticle(e);
            mediaArticleRepository.save(mediaArticle);
        }
        for(ArticleTag articleTag: articleTags){
            articleTag.getPk().setArticle(e);
            articleTagRepository.save(articleTag);
        }
        return e;

    }

    @Override
    public Events updateEvents(Events events) {
        Events e = eventsRepository.getOne(events.getId());
        if(e == null){
            throw new ResourceNotFoundException("événement introuvable");
        }
        List<MediaArticle> mediaArticles = new ArrayList<>(events.getMediaArticles());
        events.setMediaArticles(new ArrayList<>());
        events.setArticleTags(e.getArticleTags());
        // article = articleRepository.saveAndFlush(article);
        for(MediaArticle mediaArticle: mediaArticles){
            mediaArticle.getPk().setArticle(events);
            mediaArticleRepository.save(mediaArticle);
        }
        return eventsRepository.saveAndFlush(events);
        /**
        events.setMediaArticles(e.getMediaArticles());
        events.setArticleTags(e.getArticleTags());
        return eventsRepository.saveAndFlush(events);
        */
    }

    @Override
    public void deleteEvents(Long id) {
        Events  events = eventsRepository.getOne(id);
        if (events == null){
            throw new ResourceNotFoundException("l'id n'est pas bon");
        }
        eventsRepository.deleteById(id);
    }

    @Override
    public List<Events> getAllEvents()
    {List<Events> inverse;
        inverse = eventsRepository.findAll();
        Collections.reverse(inverse);
        return inverse;
    }


    @Override
    public List<Media> getMediasByEvent(Long id) {
        Events events = eventsRepository.getOne(id);
        List<MediaArticle> mediaArticles = events.getMediaArticles();
        List<Media> medias = mediaArticles.stream().map(mediaArticle -> mediaArticle.getPk().getMedia()).collect(Collectors.toList());
        return medias;
    }

    @Override
    public List<Events> getEventsPublies() {
        return eventsRepository.findAllByStateAndDatePublicationBefore(true,LocalDateTime.now());
    }

}
