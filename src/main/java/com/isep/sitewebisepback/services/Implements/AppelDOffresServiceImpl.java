package com.isep.sitewebisepback.services.Implements;

import com.isep.sitewebisepback.exceptions.BadRequestException;
import com.isep.sitewebisepback.exceptions.ResourceNotFoundException;
import com.isep.sitewebisepback.model.*;
import com.isep.sitewebisepback.repository.AppelDOffresRepository;
import com.isep.sitewebisepback.repository.MediaArticleRepository;
import com.isep.sitewebisepback.repository.MediaRepository;
import com.isep.sitewebisepback.repository.TagRepository;
import com.isep.sitewebisepback.services.AppelDOffresService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
class AppelDOffresServiceImpl implements AppelDOffresService {
    @Autowired
    private AppelDOffresRepository appelDOffresRepository;
    @Autowired
    private MediaRepository mediaRepository;
    @Autowired
    private TagRepository tagRepository;
    @Autowired
    private MediaArticleRepository mediaArticleRepository;


    @Override
    public AppelDOffres getAppelDOffresById(Long id) {
        AppelDOffres appelDOffres = appelDOffresRepository.getOne(id);
        if (appelDOffres == null){
            throw new ResourceNotFoundException("l'id n'est pas bon");
        }
        return appelDOffres;
    }

    @Override
    public AppelDOffres saveAppelDOffres(AppelDOffres appelDOffres) {
        List<MediaArticle> mediaArticles = new ArrayList<>(appelDOffres.getMediaArticles());
        appelDOffres.setMediaArticles(new ArrayList<>());
        appelDOffres.setDateCreation(LocalDateTime.now());
        AppelDOffres a = appelDOffresRepository.save(appelDOffres);
        for(MediaArticle mediaArticle: mediaArticles){
            mediaArticle.getPk().setArticle(a);
            mediaArticleRepository.save(mediaArticle);
        }
        return a;

    }

    @Override
    public AppelDOffres updateAppelDOffres(AppelDOffres appelDOffres) {
        AppelDOffres a = appelDOffresRepository.getOne(appelDOffres.getId());
        if(a == null){
            throw new ResourceNotFoundException("annonce introuvable");
        }
        List<MediaArticle> mediaArticles = new ArrayList<>(appelDOffres.getMediaArticles());
        appelDOffres.setMediaArticles(new ArrayList<>());
        appelDOffres.setArticleTags(a.getArticleTags());
        // article = articleRepository.saveAndFlush(article);
        for(MediaArticle mediaArticle: mediaArticles){
            mediaArticle.getPk().setArticle(appelDOffres);
            mediaArticleRepository.save(mediaArticle);
        }
        return appelDOffresRepository.saveAndFlush(appelDOffres);
        /**
        appelDOffres.setMediaArticles(a.getMediaArticles());
        appelDOffres.setArticleTags(a.getArticleTags());
        return appelDOffresRepository.saveAndFlush(appelDOffres);
         */

    }


    @Override
    public void deleteAppelDOffres(Long id) {
        AppelDOffres appelDOffres = appelDOffresRepository.getOne(id);
        if (appelDOffres == null){
            throw new ResourceNotFoundException("l'id n'est pas bon");
        }
        appelDOffresRepository.deleteById(id);
    }

    @Override
    public List<AppelDOffres> getAllAppelDOffres() {
        return appelDOffresRepository.findAll();
    }

    @Override
    public List<Media> getMediasByAppelDOffres(Long id) {
        AppelDOffres appelDOffres = appelDOffresRepository.getOne(id);
        List<MediaArticle> mediaArticles = appelDOffres.getMediaArticles();
        List<Media> medias = mediaArticles.stream().map(mediaArticle -> mediaArticle.getPk().getMedia()).collect(Collectors.toList());
        return medias;
    }

    @Override
    public List<AppelDOffres> getAppelsPublies() {
        return appelDOffresRepository.findAllByStateAndDatePublicationBefore(true,LocalDateTime.now());
    }


}
