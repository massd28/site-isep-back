package com.isep.sitewebisepback.services.Implements;
import com.isep.sitewebisepback.exceptions.BadRequestException;
import com.isep.sitewebisepback.exceptions.ResourceNotFoundException;
import com.isep.sitewebisepback.model.Article;
import com.isep.sitewebisepback.model.Media;
import com.isep.sitewebisepback.model.*;
import com.isep.sitewebisepback.repository.*;
import com.isep.sitewebisepback.services.ArticleService;
import com.isep.sitewebisepback.services.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;


import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

@Service
@Transactional
public class ArticleServiceImpl implements ArticleService {
    @Autowired
    private ArticleRepository articleRepository;
    @Autowired
    private MediaRepository mediaRepository;
    @Autowired
    private ArticleTagRepository articleTagRepository;
    @Autowired
    private TagRepository tagRepository;
    @Autowired
    private MediaArticleRepository mediaArticleRepository;
    @Autowired
    private PartenaireRepository partenaireRepository;
    private Random rand;

    @Override
    public Article getArticleById(Long id) {
        Article article = articleRepository.getOne(id);
        if(article == null){
            throw new ResourceNotFoundException("article introuvable");
        }
        return article;
    }

    @Override
    public Article saveArticle(Article article) {
        List<MediaArticle> mediaArticles = new ArrayList<>(article.getMediaArticles());
        List<ArticleTag> articleTags = new ArrayList<>(article.getArticleTags());
        article.setMediaArticles(new ArrayList<>());
        article.setArticleTags(new ArrayList<>());
        article.setDateCreation(LocalDateTime.now());
        Article a = articleRepository.save(article);
        for(MediaArticle mediaArticle: mediaArticles){
            mediaArticle.getPk().setArticle(a);
            mediaArticleRepository.save(mediaArticle);
        }
        for(ArticleTag articleTag: articleTags){
            articleTag.getPk().setArticle(a);
            articleTagRepository.save(articleTag);
        }
        return a;
    }

    @Override
    public Article updateArticle(Article article) {
        Article a = articleRepository.getOne(article.getId());
        if(a == null){
            throw new ResourceNotFoundException("article introuvable");
        }
        List<MediaArticle> mediaArticles = new ArrayList<>(article.getMediaArticles());
        List<ArticleTag> articleTags = new ArrayList<>(article.getArticleTags());
        article.setMediaArticles(new ArrayList<>());
        article.setArticleTags(new ArrayList<>());

        for(MediaArticle mediaArticle: mediaArticles){
            mediaArticle.getPk().setArticle(article);
            mediaArticleRepository.save(mediaArticle);
        }
        for(ArticleTag articleTag: articleTags){
            articleTag.getPk().setArticle(article);
            articleTagRepository.save(articleTag);
        }
        return articleRepository.saveAndFlush(article);
    }



    @Override
    public void deleteArticle(Long id) {
        Article article = articleRepository.getOne(id);
        if(article == null){
            throw new ResourceNotFoundException("article introuvable");
        }
        articleRepository.deleteById(id);
    }

    @Override
    public List<Article> getAllArticles()
    {
        List<Article> inverse;
        inverse = articleRepository.findAll();
        Collections.reverse(inverse);
        return inverse;
    }

    @Override
    public List<Article> getNews(){
        List<Article> inverse;
        inverse = articleRepository.findAllByNews(true);
        Collections.reverse(inverse);
        return inverse;
    }


    @Override
    public List<Article> getArticleByTagName(String nomSearch) {
        Tag tag = tagRepository.findByNom(nomSearch);

        List<ArticleTag> articleTags = tag.getArticlesTags();
        List<Article> articles = articleTags.stream().map(articleTag -> articleTag.getPk().getArticle()).collect(Collectors.toList());
        return articles;
     }

    @Override
    public List<Media> getMediasByArticle(Long id) {
        Article article = articleRepository.getOne(id);
        List<MediaArticle> mediaArticles = article.getMediaArticles();
        List<Media> medias = mediaArticles.stream().map(mediaArticle -> mediaArticle.getPk().getMedia()).collect(Collectors.toList());
        return medias;
    }

    @Override
    public List<Article> getArticlesByPage(String page) {
        return articleRepository.findAllByPageCible(page);
    }

    @Override
    public List<Article> getArticlesPublies() {
        List<Article> articlesPublies = articleRepository.findAllByStateAndDatePublicationBeforeAndNews(true,LocalDateTime.now(),true);
        Collections.reverse(articlesPublies);
        return articlesPublies;
    }

    @Override
    public void deleteMedia(Long idMedia,Long idArticle) {
        Article article = articleRepository.getOne(idArticle);
        Media media = mediaRepository.getOne(idMedia);
        MediaArticlePk mediaArticlePk = new MediaArticlePk();
        mediaArticlePk.setMedia(media);
        mediaArticlePk.setArticle(article);
        MediaArticle mediaArticle = mediaArticleRepository.getOne(mediaArticlePk);
        if (mediaArticle != null) {
            mediaArticleRepository.delete(mediaArticle);
        }
        else {
            throw new ResourceNotFoundException("article introuvable ou media introuvable");
        }
    }

    @Override
    public List<Article> getArticlesByPartenaire(Long idPart) {
        Partenaire partenaire = partenaireRepository.getOne(idPart);
        if(partenaire == null){
            throw new ResourceNotFoundException("article introuvable");
        }
        return articleRepository.findAllByPartenaire(partenaire);
    }

    @Override
    public List<Article> getArticlesEcoPublies(String page) {
        return articleRepository.findAllByPageCibleAndStateAndDatePublicationBefore(page,true,LocalDateTime.now());
    }

}
