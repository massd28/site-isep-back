package com.isep.sitewebisepback.services.Implements;

import com.isep.sitewebisepback.exceptions.BadRequestException;
import com.isep.sitewebisepback.exceptions.ResourceNotFoundException;
import com.isep.sitewebisepback.model.Partenaire;
import com.isep.sitewebisepback.model.User;
import com.isep.sitewebisepback.repository.PartenaireRepository;
import com.isep.sitewebisepback.services.PartenaireService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
@Service
@Transactional
public class PartenaireServiceImpl implements PartenaireService {

    @Autowired
    private PartenaireRepository partenaireRepository;

    @Override
    public Partenaire getPartenaireById(Long id) {
        Partenaire partenaire = partenaireRepository.getOne(id);
        if (partenaire == null){
            throw new ResourceNotFoundException("l'id n'est pas bon");
        }
        return partenaireRepository.getOne(id);
    }

    @Override
    public Partenaire savePartenaire(Partenaire partenaire) {
        return partenaireRepository.save(partenaire);
    }

    @Override
    public Partenaire updatePartenaire(Partenaire partenaire) {
        return partenaireRepository.saveAndFlush(partenaire);
    }

    @Override
    public void deletePartenaire(Long id) {
        Partenaire partenaire = partenaireRepository.getOne(id);
        if (partenaire == null){
            throw new ResourceNotFoundException("l'id n'est pas bon");
        }
        partenaireRepository.deleteById(id);

    }

    @Override
    public List<Partenaire> getAllPartenaires() {
        return partenaireRepository.findAll();
    }
}
