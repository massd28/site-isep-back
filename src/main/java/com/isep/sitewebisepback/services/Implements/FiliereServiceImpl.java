package com.isep.sitewebisepback.services.Implements;

import com.isep.sitewebisepback.exceptions.BadRequestException;
import com.isep.sitewebisepback.exceptions.ResourceNotFoundException;
import com.isep.sitewebisepback.model.Departement;
import com.isep.sitewebisepback.model.Filiere;
import com.isep.sitewebisepback.model.Media;
import com.isep.sitewebisepback.repository.DepartementRepository;
import com.isep.sitewebisepback.repository.FiliereRepository;
import com.isep.sitewebisepback.services.FiliereService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Service
@Transactional
public class FiliereServiceImpl implements FiliereService {
    @Autowired
    private FiliereRepository filiereRepository;
    private DepartementRepository departementRepository;

    @Override
    public Filiere getFiliereById(Long id) {
        Filiere  filiere = filiereRepository.getOne(id);
        if (filiere == null){
            throw new ResourceNotFoundException("l'id n'est pas bon");
        }
        return filiere;
    }

    @Override
    public Filiere saveFiliere(Filiere filiere) {
        if (filiere == null || filiere.getDepartement() == null) {
            throw new ResourceNotFoundException("Renseigner le dept !");
        }

        return filiereRepository.save(filiere);
    }

    @Override
    public Filiere updateFiliere(Filiere filiere) {
        return filiereRepository.saveAndFlush(filiere);
    }

    @Override
    public void deleteFiliere(Long id) {
        Filiere  filiere = filiereRepository.getOne(id);
        if (filiere == null){
            throw new ResourceNotFoundException("l'id n'est pas bon");
        }
        filiereRepository.deleteById(id);
    }

    @Override
    public List<Filiere> getAllFilieres() {
        return filiereRepository.findAll();
    }

    @Override
    public List<Filiere> getFilieresByDepartement(Long id) {
        Departement departement = departementRepository.getOne(id);
        return departement.getFilieres();
    }

}
