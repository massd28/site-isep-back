package com.isep.sitewebisepback.services.Implements;

import com.isep.sitewebisepback.exceptions.ResourceNotFoundException;
import com.isep.sitewebisepback.model.Allocution;
import com.isep.sitewebisepback.repository.AllocutionRepository;
import com.isep.sitewebisepback.services.AllocutionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

@Service
@Transactional
public class AllocutionServiceImpl implements AllocutionService {

    @Autowired
    private AllocutionRepository allocutionRepository;

    @Override
    public Allocution getAllocutionById(Long id) {
        Allocution allocution = allocutionRepository.getOne(id);
        if(allocution == null){
            throw new ResourceNotFoundException("allocution introuvable");
        }
        return allocution;
    }

    @Override
    public Allocution updateAllocution(Allocution allocution) {
        return allocutionRepository.saveAndFlush(allocution);
    }

    @Override
    public Allocution saveAllocution(Allocution allocution) {

        return allocutionRepository.save(allocution);
    }

    @Override
    public void deleteAllocution(Long id) {
        Allocution allocution = allocutionRepository.getOne(id);
        if(allocution == null){
            throw new ResourceNotFoundException("allocution introuvable");
        }

        allocutionRepository.delete(allocution);
    }

    @Override
    public List<Allocution> getAllAllocutions() {
        return allocutionRepository.findAll();
    }

    @Override
    public Allocution getRandomAllocution() {
        List<Allocution> allocutions = allocutionRepository.findAll();
        return allocutions.get(ThreadLocalRandom.current().nextInt(0, allocutions.size()));
    }
}
