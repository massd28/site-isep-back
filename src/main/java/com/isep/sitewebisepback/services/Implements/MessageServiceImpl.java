package com.isep.sitewebisepback.services.Implements;

import com.isep.sitewebisepback.exceptions.ResourceNotFoundException;
import com.isep.sitewebisepback.model.*;
import com.isep.sitewebisepback.repository.MediaRepository;
import com.isep.sitewebisepback.repository.MessageRepository;
import com.isep.sitewebisepback.services.MediaService;
import com.isep.sitewebisepback.services.MessageService;
import com.isep.sitewebisepback.services.SimpleEmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


@Service
@Transactional
public class MessageServiceImpl implements MessageService {

    @Autowired
    private MessageRepository  messageRepository;

    @Autowired
    private MediaRepository  mediaRepository;

    @Autowired
    private MediaService mediaService;
    @Autowired
    private MessageService messageService;
    @Autowired
    private SimpleEmailService simpleEmailService;

    @Override
    public Message getMessageById(Long id) {
        Message message = messageRepository.getOne(id);
        if (message == null) {
            throw new ResourceNotFoundException("l'id n'est pas bon");
        }
        return messageRepository.getOne(id);
    }

    @Override
    public Message saveMessage(Message message) {

        return messageRepository.save(message);
    }

    @Override
    public Message updateMessage(Message message) {
        return messageRepository.saveAndFlush(message);
    }

    @Override
    public void deleteMessage(Long id) {
        Message message = messageRepository.getOne(id);
        if (message == null) {
            throw new ResourceNotFoundException("l'id n'est pas bon");
        }
        messageRepository.deleteById(id);
    }

    @Override
    public List<Message> getAllMessage() {
        return messageRepository.findAll();
    }

    @Override
    public List<Message> getMessagesSent(){
        List<Message> inverse;
        inverse = messageRepository.findAllBySent(true);
        Collections.reverse(inverse);
        return inverse;
    }

    @Override
    public List<Message> getMessagesReceived(){
        List<Message> inverse;
        inverse = messageRepository.findAllByReceived(true);
        Collections.reverse(inverse);
        return inverse;
    }

    @Override
    public List<Message> getMessagesLu(){
        List<Message> inverse;
        inverse = messageRepository.findAllByLuAndReceived(true, true);
        Collections.reverse(inverse);
        return inverse;
    }

    @Override
    public List<Message> getMessagesNonLu(){
        List<Message> inverse;
        inverse = messageRepository.findAllByLuAndReceived(false, true);
        Collections.reverse(inverse);
        return inverse;
    }
}
