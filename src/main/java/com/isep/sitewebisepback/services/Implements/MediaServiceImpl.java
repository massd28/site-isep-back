package com.isep.sitewebisepback.services.Implements;

import com.isep.sitewebisepback.utils.UtilBase64Image;
import com.isep.sitewebisepback.exceptions.ResourceNotFoundException;
import com.isep.sitewebisepback.model.Media;
import com.isep.sitewebisepback.repository.MediaRepository;
import com.isep.sitewebisepback.services.MediaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class MediaServiceImpl implements MediaService {
    @Autowired
    private MediaRepository mediaRepository;
    @Value("${pathFile}")
    private String pathFile;
    @Value("${source}")
    private String sourceFile;

    @Override
    public Media getMediaById(Long id) {

        Media media = mediaRepository.getOne(id);
        if (media == null) {
            throw new ResourceNotFoundException("l'id n'est pas bon");
        }
        return mediaRepository.getOne(id);
    }

    @Override
    public Media saveMedia(Media media) {
        return mediaRepository.save(media);
    }

    @Override
    public Media updateMedia(Media media) {
        return mediaRepository.saveAndFlush(media);
    }

    @Override
    public void deleteMedia(Long id) {
        Media media = mediaRepository.getOne(id);
        if (media == null) {
            throw new ResourceNotFoundException("l'id n'est pas bon");
        }
        mediaRepository.deleteById(id);
    }

    @Override
    public List<Media> getAllMedias() {
        return mediaRepository.findAll();
}


    public List<Media> createMedia(List<Media> medias) {
        List<Media> medias2 = new ArrayList<>();
        for (Media media: medias) {
            File dossier = new File("C:/wamp/www/medias/");
            if (!dossier.exists()) {
                dossier.mkdir();
            }
            if (!dossier.isDirectory()) {
                dossier.mkdir();
            }

            Date date = new Date();
            long currentTime = date.getTime();

            String source = +currentTime + ".jpeg";

            if (UtilBase64Image.decoder(media.getUrl(), pathFile + currentTime + "." + media.getType())) {
                media.setUrl("/medias/" + currentTime + "." + media.getType());


            }
            media = mediaRepository.save(media);
            medias2.add(media);
            //return null;
        }
        return medias2;
    }
}
