package com.isep.sitewebisepback.services;
import com.isep.sitewebisepback.model.AppelDOffres;
import com.isep.sitewebisepback.model.Media;


import java.util.List;

public interface AppelDOffresService {
    AppelDOffres getAppelDOffresById(Long id);
    AppelDOffres saveAppelDOffres(AppelDOffres appelDOffres);
    AppelDOffres updateAppelDOffres (AppelDOffres appelDOffres);
    void deleteAppelDOffres (Long id);
    List<AppelDOffres> getAllAppelDOffres();
    List<Media> getMediasByAppelDOffres (Long id);
    List<AppelDOffres> getAppelsPublies();

}