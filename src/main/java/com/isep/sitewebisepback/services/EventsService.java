package com.isep.sitewebisepback.services;
import com.isep.sitewebisepback.model.Events;
import com.isep.sitewebisepback.model.Events;
import com.isep.sitewebisepback.model.Media;
import com.isep.sitewebisepback.model.Tag;


import java.util.List;

public interface EventsService {
   Events getEventsById(Long id);
    Events saveEvents(Events events);
    Events updateEvents (Events events);
    void deleteEvents (Long id);
    List<Events> getAllEvents();
    public List<Media> getMediasByEvent(Long id);
    List<Events> getEventsPublies();

}