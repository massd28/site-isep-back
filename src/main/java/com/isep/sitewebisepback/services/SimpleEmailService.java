package com.isep.sitewebisepback.services;
import com.isep.sitewebisepback.model.Message;
import com.isep.sitewebisepback.model.User;

import java.util.List;

public interface SimpleEmailService {
    void sendEmail(Message mail) throws Exception;
}
