package com.isep.sitewebisepback;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SiteWebIsepBackApplication {

    public static void main(String[] args) {
        SpringApplication.run(SiteWebIsepBackApplication.class, args);
    }
}
