package com.isep.sitewebisepback.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;


@Data
@Entity
public class Allocution implements Serializable {
    @Id
    @GeneratedValue
    private Long id;
    @NotNull
    private String titre;
    @Column(columnDefinition = "text")
    private String corps;
    @OneToOne
    private Media media;
}
