package com.isep.sitewebisepback.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Entity
@Data
public class Tag implements Serializable {

    @Id
    @GeneratedValue
    private Long id;
    @NotNull
    @Column(unique = true)
    private String nom;
    @OneToMany(mappedBy = "pk.tag", cascade = CascadeType.ALL)
    @JsonIgnore
    private List<ArticleTag> articlesTags;

}
