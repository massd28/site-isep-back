package com.isep.sitewebisepback.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Data
@PrimaryKeyJoinColumn(name = "id")
public class Events extends Article  {

    private LocalDateTime dateDebut;
    private LocalDateTime dateFin;
    private String lieu;

}
