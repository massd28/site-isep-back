package com.isep.sitewebisepback.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Data
@Entity
public class Departement implements Serializable {
    @Id
    @GeneratedValue
    private Long id;
    @NotNull(message = "Champ non vide")
    @NotBlank
    private String nom;
    @Lob
    @NotBlank
    @Column(columnDefinition = "text")
    private String description;
    @JsonIgnore
    @OneToMany(mappedBy = "departement")
    private List<Filiere> filieres;

    @ManyToOne
    private User user;
    @JsonIgnore
    @OneToMany(mappedBy = "departement")
    private List<Media> medias;
}
