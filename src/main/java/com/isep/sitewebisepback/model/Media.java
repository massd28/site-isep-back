package com.isep.sitewebisepback.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.isep.sitewebisepback.utils.CustomCheminSerializer;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;


@Entity
@Data
public class Media implements Serializable {
    @Id
    @GeneratedValue
    private Long id;
    private String nom;
    @NotNull (message = "Veuillez renseigner l'url du media")
    @JsonSerialize(using = CustomCheminSerializer.class)
    @Column(columnDefinition = "text")
    private String url;
    private String type;
    @JsonIgnore
    @OneToMany(mappedBy = "pk.media", cascade = CascadeType.ALL)
    private List<MediaArticle> mediaArticles ;
    @OneToOne
    private Allocution allocution;
    @OneToOne
    private Partenaire partenaire;
    @ManyToOne
    private Departement departement;
    @ManyToOne
    @JsonIgnore
    private Message message;
}


