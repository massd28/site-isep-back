package com.isep.sitewebisepback.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Entity
public class Message implements Serializable {
    @Id
    @GeneratedValue
    private Long id;
    private String objet;
    @NotNull
    @Column(columnDefinition = "text")
    private String corps;
    private String nameEmetteur;
    @NotNull
    private String emailEmetteur;
    private String emailDestinataire;
    private String phoneEmetteur;
    private String companyEmetteur;
    private LocalDateTime dateEnvoi;
    private boolean lu;
    private boolean sent;
    private boolean received;
    @OneToMany(mappedBy = "message", cascade = CascadeType.ALL)
    private List<Media> medias;

}