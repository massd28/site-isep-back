package com.isep.sitewebisepback.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Data
@PrimaryKeyJoinColumn(name = "id")
public class AppelDOffres extends Article  {

    private LocalDateTime dateCloture;
}
