package com.isep.sitewebisepback.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Embeddable
@Data
public class ArticleTagPk implements Serializable {
    @ManyToOne
    @JsonIgnore
    private Article article;
    @ManyToOne
    private Tag tag;
}
