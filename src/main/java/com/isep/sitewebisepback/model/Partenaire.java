package com.isep.sitewebisepback.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Data
@Entity
public class Partenaire implements Serializable {
    @Id
    @GeneratedValue
    private Long id;
    @NotNull
    private String nom;
    private String siteWeb;
    @Column(columnDefinition = "text")
    private String descriptionPartenariat;
    @OneToOne
    private Media media;
    @OneToMany(mappedBy = "partenaire", cascade = CascadeType.ALL)
    @JsonIgnore
    private List<Article> articles;
    @ManyToOne
    private User user;

}
