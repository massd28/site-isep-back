package com.isep.sitewebisepback.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@Entity
public class Filiere implements Serializable {
    @Id
    @GeneratedValue
    private Long id;
    @NotNull(message = "Champ non vide")
    private String nom;
    @Lob
    @NotNull
    @Column(columnDefinition = "text")
    private String presentation;
    @Column(columnDefinition = "text")
    private String objectif;
    @Column(columnDefinition = "text")
    private String conditionAdmission;
    @Column(columnDefinition = "text")
    private String debouches;
    @ManyToOne
    private Departement departement;
    @ManyToOne
    private User user;
}
