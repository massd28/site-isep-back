package com.isep.sitewebisepback.model;


import lombok.Data;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.io.Serializable;

@Entity
@Data
public class MediaArticle implements Serializable {
    @EmbeddedId
    private MediaArticlePk pk;
}