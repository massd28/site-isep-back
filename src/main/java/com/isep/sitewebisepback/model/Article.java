package com.isep.sitewebisepback.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Date;
import java.util.List;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Data
@Inheritance(strategy = InheritanceType.JOINED)
public class Article implements Serializable {
@Id
@GeneratedValue
private Long id;
@NotNull(message = "Veuillez mettre un titre")
private String titre;
@Lob
@NotNull(message = "Veuillez mettre un contenu")
@Column(columnDefinition = "text")
private String corps;
private LocalDateTime dateCreation;
private LocalDateTime datePublication;
private boolean state;
private boolean news;
private String pageCible;
private int vues;
@OneToMany(mappedBy = "pk.article", cascade = CascadeType.ALL)
private List<ArticleTag> articleTags;
@OneToMany(mappedBy = "pk.article", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
private List<MediaArticle> mediaArticles ;
@ManyToOne
private Partenaire partenaire;

}
