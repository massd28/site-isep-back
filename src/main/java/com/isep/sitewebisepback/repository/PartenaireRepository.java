package com.isep.sitewebisepback.repository;

import com.isep.sitewebisepback.model.Partenaire;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PartenaireRepository extends JpaRepository<Partenaire,Long> {
}
