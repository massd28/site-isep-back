package com.isep.sitewebisepback.repository;

import com.isep.sitewebisepback.model.ArticleTag;

import com.isep.sitewebisepback.model.ArticleTagPk;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ArticleTagRepository extends JpaRepository<ArticleTag, ArticleTagPk> {
}
