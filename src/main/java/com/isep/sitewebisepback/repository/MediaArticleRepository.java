package com.isep.sitewebisepback.repository;

import com.isep.sitewebisepback.model.MediaArticle;
import com.isep.sitewebisepback.model.MediaArticlePk;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MediaArticleRepository extends JpaRepository<MediaArticle, MediaArticlePk> {
}
