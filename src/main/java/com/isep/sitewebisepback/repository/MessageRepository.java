package com.isep.sitewebisepback.repository;

import com.isep.sitewebisepback.model.Allocution;
import com.isep.sitewebisepback.model.Article;
import com.isep.sitewebisepback.model.Message;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MessageRepository extends JpaRepository<Message, Long> {
    List<Message> findAllBySent(boolean isSent);
    List<Message> findAllByReceived(boolean isReceived);
    List<Message> findAllByLuAndReceived(boolean isLu, boolean isReceived);

}
