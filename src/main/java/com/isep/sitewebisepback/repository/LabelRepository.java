package com.isep.sitewebisepback.repository;

import com.isep.sitewebisepback.model.Filiere;
import com.isep.sitewebisepback.model.Label;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LabelRepository extends JpaRepository<Label,Long> {
}
