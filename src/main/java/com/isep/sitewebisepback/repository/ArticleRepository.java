package com.isep.sitewebisepback.repository;


import com.isep.sitewebisepback.model.Article;
import com.isep.sitewebisepback.model.Partenaire;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface ArticleRepository extends JpaRepository<Article, Long> {
    List<Article> findAllByNews(boolean isNewed);
    List<Article> findAllByPageCible(String pageCible);
    List<Article> findAllByStateAndDatePublicationBeforeAndNews(boolean state, LocalDateTime localDateTime, boolean isNewed);
    List<Article> findAllByPartenaire(Partenaire partenaire);
    List<Article> findAllByPageCibleAndStateAndDatePublicationBefore(String page, boolean state, LocalDateTime localDateTime);
}