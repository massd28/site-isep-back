package com.isep.sitewebisepback.repository;


import com.isep.sitewebisepback.model.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TagRepository extends JpaRepository<Tag, Long>{
    Tag findByNom(String nom);
}
