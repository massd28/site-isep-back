package com.isep.sitewebisepback.repository;



import com.isep.sitewebisepback.model.AppelDOffres;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface AppelDOffresRepository extends JpaRepository<AppelDOffres, Long> {
    List<AppelDOffres> findAllByStateAndDatePublicationBefore(boolean state, LocalDateTime localDateTime);
}