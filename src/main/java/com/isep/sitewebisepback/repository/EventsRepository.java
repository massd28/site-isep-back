package com.isep.sitewebisepback.repository;



import com.isep.sitewebisepback.model.Events;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface EventsRepository extends JpaRepository<Events, Long> {
    List<Events> findAllByStateAndDatePublicationBefore(boolean state, LocalDateTime localDateTime);
}