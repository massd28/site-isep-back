package com.isep.sitewebisepback.repository;

import com.isep.sitewebisepback.model.Allocution;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AllocutionRepository extends JpaRepository<Allocution, Long> {
}
