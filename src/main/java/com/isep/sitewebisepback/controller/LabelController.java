package com.isep.sitewebisepback.controller;


import com.isep.sitewebisepback.model.Allocution;
import com.isep.sitewebisepback.model.Label;
import com.isep.sitewebisepback.services.AllocutionService;
import com.isep.sitewebisepback.services.LabelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin(origins="*")
public class LabelController {
    @Autowired
    private LabelService labelService;

    @GetMapping(value = "/labels", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Label> getAllLabel() {
        return labelService.getAllLabel();
    }

    @GetMapping(value = "/labels/{id}", produces  = MediaType.APPLICATION_JSON_VALUE)
    public Label getLabelById(@PathVariable Long id) {
        return labelService.getLabelById(id);
    }

    @RequestMapping(value = "/labels",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Label saveLabel(@RequestBody @Valid Label label) {
        return labelService.saveLabel(label);
    }

    @PutMapping(value = "/labels/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Label updateLabel(@RequestBody Label label, @PathVariable Long id ){
        label.setId(id);
        return labelService.updateLabel(label);
    }

    @DeleteMapping(value = "/labels/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteLabel(@PathVariable Long id) {
        labelService.deleteLabel(id);
    }

}
