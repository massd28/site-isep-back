package com.isep.sitewebisepback.controller;

import com.isep.sitewebisepback.model.Filiere;
import com.isep.sitewebisepback.services.FiliereService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class FiliereController {
    @Autowired
    private FiliereService filiereService;

    @GetMapping(value = "/filieres", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Filiere> getAllFilieres(){
        return filiereService.getAllFilieres();
    }

    @GetMapping(value = "/filieres/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Filiere getFiliereById(@PathVariable Long id){
        return filiereService.getFiliereById(id);
    }

    @RequestMapping(value = "/filieres",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Filiere saveFiliere(@RequestBody Filiere filiere){
        return filiereService.saveFiliere(filiere);
    }

    @PutMapping(value = "/filieres/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Filiere updateFiliere(@RequestBody Filiere filiere,@PathVariable Long id){
        filiere.setId(id);
        return filiereService.updateFiliere(filiere);
    }

    @DeleteMapping(value = "/filieres/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteFiliere(@PathVariable Long id){
        filiereService.deleteFiliere(id);
    }
}
