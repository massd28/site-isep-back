package com.isep.sitewebisepback.controller;

import com.isep.sitewebisepback.model.Article;
import com.isep.sitewebisepback.model.Media;
import com.isep.sitewebisepback.model.Tag;
import com.isep.sitewebisepback.services.ArticleService;
import com.isep.sitewebisepback.services.MediaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class ArticleController {
    @Autowired
    private ArticleService articleService;
    @Autowired
    private MediaService mediaService;

    @GetMapping(value = "/articles", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Article> getAllArticles() {
        return articleService.getAllArticles();
    }

    @GetMapping(value = "/articles/{id}", produces  = MediaType.APPLICATION_JSON_VALUE)
    public Article getArticleById(@PathVariable Long id) {
        return articleService.getArticleById(id);
    }

    @RequestMapping(value = "/articles",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Article saveArticle(@RequestBody @Valid Article article) {
        //Media media = mediaService.createMedia(article.getMedia());
        Article article1 = articleService.saveArticle(article);
        //article1.setMedia(media);
        return articleService.updateArticle(article1);
    }

    @PutMapping(value = "/articles/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Article updateArticle(@RequestBody Article article, @PathVariable Long id ){
        article.setId(id);
        return articleService.updateArticle(article);
    }


    @DeleteMapping(value = "/articles/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteArticle(@PathVariable Long id) {
        articleService.deleteArticle(id);
    }

    @GetMapping(value = "/articles/tags/{searchName}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Article> getArticleByTagName(@PathVariable String searchName) {
        return articleService.getArticleByTagName(searchName);
    }
    @GetMapping(value = "/articles/{id}/medias", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Media> getMediasByArticle(@PathVariable Long id) {
        return articleService.getMediasByArticle(id);
    }

    @GetMapping(value = "/articles/news", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Article> getAllNews() {
        return articleService.getNews();
    }

    @GetMapping(value = "/articles/pagescibles/{page}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Article> getArticlesByPage(@PathVariable String page) {
        return articleService.getArticlesByPage(page);
    }

    @GetMapping(value = "/articles/publies", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Article> getArticlesPublies() {
        return articleService.getArticlesPublies();
    }
    @DeleteMapping(value = "/articles/{idArticle}/suppression-media/{idMedia}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteMedia( @PathVariable Long idMedia,@PathVariable Long idArticle) {
        articleService.deleteMedia(idMedia,idArticle);
    }
    @GetMapping(value = "/articles/partenaires/{idPart}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Article> getArticlesByPartenaire(@PathVariable Long idPart) {
        return articleService.getArticlesByPartenaire(idPart);
    }
    @GetMapping(value = "/articles/publies/pagescibles/{page}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Article> getArticlesPubliesByPage(@PathVariable String page) {
        return articleService.getArticlesEcoPublies(page);
    }
}
