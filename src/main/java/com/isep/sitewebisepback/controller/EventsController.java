package com.isep.sitewebisepback.controller;

import com.isep.sitewebisepback.model.Article;
import com.isep.sitewebisepback.model.Events;
import com.isep.sitewebisepback.model.Media;
import com.isep.sitewebisepback.model.Tag;
import com.isep.sitewebisepback.services.EventsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class EventsController {
    @Autowired
    private EventsService eventsService;

    @GetMapping(value = "/events", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Events> getAllEvents() {
        return eventsService.getAllEvents();
    }

    @GetMapping(value = "/events/publies", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Events> getAllEventsPublies() {
        return eventsService.getEventsPublies();
    }


    @GetMapping(value = "/events/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Events getEventsById(@PathVariable Long id) {
        return eventsService.getEventsById(id);
    }

    @PostMapping(value = "/events", produces = MediaType.APPLICATION_JSON_VALUE)
    public Events saveEvents(@RequestBody Events events) {
        Events event1 = eventsService.saveEvents(events);
        //article1.setMedia(media);
        return eventsService.updateEvents(event1);
    }

    @PutMapping(value = "/events/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Events updateEvents(@RequestBody Events events, @PathVariable Long id ){
        events.setId(id);
        return eventsService.updateEvents(events);
    }

    @DeleteMapping(value = "/events/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteEvents(@PathVariable Long id) {
        eventsService.deleteEvents(id);
    }

    @GetMapping(value = "/events/{id}/medias", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Media> getMediasByEvent(@PathVariable Long id) {
        return eventsService.getMediasByEvent(id);
    }


}
