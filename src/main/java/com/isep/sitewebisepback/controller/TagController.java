package com.isep.sitewebisepback.controller;

import com.isep.sitewebisepback.model.Tag;
import com.isep.sitewebisepback.services.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class TagController {

    @Autowired
    private TagService tagService;

    @GetMapping(value = "/tags", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Tag> getAllTags() {
        return tagService.getAllTags();
    }

    @GetMapping(value = "/tags/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Tag getTagById(@PathVariable Long id) {
        return tagService.getTagById(id);
    }

    @PostMapping(value = "/tags", produces = MediaType.APPLICATION_JSON_VALUE)
    public Tag saveTag(@RequestBody Tag tag) {
        return tagService.saveTag(tag);
    }

    @PutMapping(value = "/tags/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Tag updateTag(@RequestBody Tag tag, @PathVariable Long id ){
        tag.setId(id);
        return tagService.updateTag(tag);
    }

    @DeleteMapping(value = "/tags/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteTag(@PathVariable Long id) {
        tagService.deleteTag(id);
    }
}
