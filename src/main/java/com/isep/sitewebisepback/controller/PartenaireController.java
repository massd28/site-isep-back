package com.isep.sitewebisepback.controller;

import com.isep.sitewebisepback.model.Partenaire;
import com.isep.sitewebisepback.services.PartenaireService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class PartenaireController {
    @Autowired
    PartenaireService partenaireService;

    @GetMapping(value = "/partenaires",produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Partenaire> getAllPartenaires(){return partenaireService.getAllPartenaires();}

    @GetMapping(value = "/partenaires/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    public Partenaire getPartenaireById(@PathVariable Long id){
        return partenaireService.getPartenaireById(id);
    }

    @PostMapping(value = "/partenaires",produces = MediaType.APPLICATION_JSON_VALUE)
    public Partenaire savePartenaire(@RequestBody Partenaire partenaire){
        return partenaireService.savePartenaire(partenaire);
    }

    @PutMapping(value = "/partenaires/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    public Partenaire updatePartenaire(@RequestBody Partenaire partenaire,@PathVariable Long id){
        partenaire.setId(id);
        return partenaireService.updatePartenaire(partenaire);
    }

    @DeleteMapping(value = "/partenaires/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    public void deletePartenaire(@PathVariable Long id){partenaireService.deletePartenaire(id);}
}
