package com.isep.sitewebisepback.controller;


import com.isep.sitewebisepback.model.Allocution;
import com.isep.sitewebisepback.services.AllocutionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin(origins="*")
public class AllocutionController {
    @Autowired
    private AllocutionService allocutionService;

    @GetMapping(value = "/allocutions", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Allocution> getAllAllocutions() {
        return allocutionService.getAllAllocutions();
    }

    @GetMapping(value = "/allocutions/{id}", produces  = MediaType.APPLICATION_JSON_VALUE)
    public Allocution getAllocutionById(@PathVariable Long id) {
        return allocutionService.getAllocutionById(id);
    }

    @RequestMapping(value = "/allocutions",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Allocution saveAllocution(@RequestBody @Valid Allocution allocution) {
        return allocutionService.saveAllocution(allocution);
    }

    @PutMapping(value = "/allocutions/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Allocution updateAllocution(@RequestBody Allocution allocution, @PathVariable Long id ){
        allocution.setId(id);
        return allocutionService.updateAllocution(allocution);
    }

    @DeleteMapping(value = "/allocutions/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteAllocution(@PathVariable Long id) {
        allocutionService.deleteAllocution(id);
    }

    @GetMapping(value = "/allocution", produces = MediaType.APPLICATION_JSON_VALUE)
    public Allocution getRandomAllocution(){return allocutionService.getRandomAllocution(); }
}
