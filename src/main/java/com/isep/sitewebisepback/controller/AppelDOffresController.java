package com.isep.sitewebisepback.controller;

import com.isep.sitewebisepback.model.AppelDOffres;
import com.isep.sitewebisepback.model.Media;
import com.isep.sitewebisepback.services.AppelDOffresService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class AppelDOffresController {
    @Autowired
    private AppelDOffresService appelDOffresService;

    @GetMapping(value = "/appelDOffres", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<AppelDOffres> getAllAppelDOffres() {
        return appelDOffresService.getAllAppelDOffres();
    }

    @GetMapping(value = "/appelDOffres/publies", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<AppelDOffres> getAllAppelDOffresPublies() {
        return appelDOffresService.getAppelsPublies();
    }

    @GetMapping(value = "/appelDOffres/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public AppelDOffres getAppelDOffresById(@PathVariable Long id) {
        return appelDOffresService.getAppelDOffresById(id);
    }

    @RequestMapping(value = "/appelDOffres", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public AppelDOffres saveAppelDOffres(@RequestBody AppelDOffres appelDOffres) {
        AppelDOffres appelDOffres1 = appelDOffresService.saveAppelDOffres(appelDOffres);
        return appelDOffresService.updateAppelDOffres(appelDOffres1);
    }

    @PutMapping(value = "/appelDOffres/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public AppelDOffres updateAppelDOffres(@RequestBody AppelDOffres appelDOffres, @PathVariable Long id ){
        appelDOffres.setId(id);
        return appelDOffresService.updateAppelDOffres(appelDOffres);
    }

    @DeleteMapping(value = "/appelDOffres/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteAppelDOffres(@PathVariable Long id) {
        appelDOffresService.deleteAppelDOffres(id);
    }

    @GetMapping(value = "/appelDOffres/{id}/medias", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Media> getMediasByArticle(@PathVariable Long id) {
        return appelDOffresService.getMediasByAppelDOffres(id);
    }


}
