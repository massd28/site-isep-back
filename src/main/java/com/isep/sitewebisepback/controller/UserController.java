package com.isep.sitewebisepback.controller;

import com.isep.sitewebisepback.model.User;
import com.isep.sitewebisepback.services.SimpleEmailService;
import com.isep.sitewebisepback.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class UserController {


    @Autowired
    private UserService userService;
    @Autowired
    private SimpleEmailService simpleEmailService;

    @GetMapping(value = "/users",produces = MediaType.APPLICATION_JSON_VALUE)
    public List<User> getAllUsers(){return userService.getAllUsers();}

    @GetMapping(value = "/users/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    public User getUserById(@PathVariable Long id){
        return userService.getUserById(id);
    }

    @RequestMapping(value = "/users",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public User saveUser(@RequestBody @Valid User user) {

        return userService.saveUser(user);
    }

    @PutMapping(value = "users/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    public User updateUser(@RequestBody User user,@PathVariable Long id){
        user.setId(id);
        return userService.updateUser(user);
    }

    @DeleteMapping(value = "/users/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteUser(@PathVariable Long id){userService.deleteUser(id);}

    @RequestMapping(value = "/login",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public User login(@RequestBody User user) {
        return userService.login(user.getUsername(),user.getPassword(),user.getRole());
    }
}
