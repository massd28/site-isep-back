package com.isep.sitewebisepback.controller;

import com.isep.sitewebisepback.model.Departement;
import com.isep.sitewebisepback.model.Filiere;
import com.isep.sitewebisepback.model.Media;
import com.isep.sitewebisepback.services.DepartementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin(origins="*")
public class DepartementController {
    @Autowired
    private DepartementService departementService;

    @GetMapping(value = "/departements", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Departement> getAllDepartement() {
        return departementService.getAllDepartements();
    }

    @GetMapping(value = "/departements/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Departement getDepartementById(@PathVariable Long id) {
        return departementService.getDepartementById(id);
    }

    @RequestMapping(value = "/departements", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Departement saveDepartement(@RequestBody @Valid Departement departement) {
        return departementService.saveDepartement(departement);
    }

    @PutMapping(value = "/departements/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Departement updateDepartement(@RequestBody Departement departement, @PathVariable Long id ){
        departement.setId(id);
        return departementService.updateDepartement(departement);
    }

    @DeleteMapping(value = "/departements/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteDepartement(@PathVariable Long id) {
        departementService.deleteDepartement(id);
    }

    @GetMapping(value = "/departements/{id}/filieres", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Filiere> getFilieresByDepartement(@PathVariable Long id) {

        return departementService.getFilieresByDepartement(id);

    }
    @GetMapping(value = "/departements/{id}/medias", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Media> getMediasByDepartement(@PathVariable Long id) {

        return departementService.getMediasByDepartement(id);

    }
}
