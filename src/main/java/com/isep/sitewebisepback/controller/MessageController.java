package com.isep.sitewebisepback.controller;


import com.isep.sitewebisepback.model.ArticleTag;
import com.isep.sitewebisepback.model.Media;
import com.isep.sitewebisepback.model.Message;
import com.isep.sitewebisepback.repository.MediaRepository;
import com.isep.sitewebisepback.services.MediaService;
import com.isep.sitewebisepback.services.MessageService;
import com.isep.sitewebisepback.services.SimpleEmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
@RestController
@CrossOrigin(origins = "*")
public class MessageController {
    @Autowired
    private MessageService messageService;
    @Autowired
    private MediaService mediaService;
    @Autowired
    private SimpleEmailService simpleEmailService;
    @Autowired
    private MediaRepository mediaRepository;
    @Value("${source}")
    private String sourceFile;

    @GetMapping(value = "/messages", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Message> getAllMessage() {
        return messageService.getAllMessage();
    }

    @GetMapping(value = "/messages-sent", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Message> getMessagesSent() {
        return messageService.getMessagesSent();
    }

    @GetMapping(value = "/messages-received", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Message> getMessagesReceived() {
        return messageService.getMessagesReceived();
    }

    @GetMapping(value = "/messages-lu", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Message> getMessagesLu() {
        return messageService.getMessagesLu();
    }

    @GetMapping(value = "/messages-non-lu", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Message> getMessagesNonLu() {
        return messageService.getMessagesNonLu();
    }

    @GetMapping(value = "/messages/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Message getMessageById(@PathVariable Long id) {
        return messageService.getMessageById(id);
    }

    @RequestMapping(value = "/messages",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Message saveMessage(@RequestBody Message message) {

        try {
            simpleEmailService.sendEmail(message);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return messageService.saveMessage(message);
    }

    @RequestMapping(value = "/postuler",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Message postuler(@RequestBody Message message) {
        List<Media> medias = new ArrayList<>(message.getMedias());
        message.setMedias(new ArrayList<>());
        String str = new String();
        if (!medias.isEmpty()) {
            List<Media>   medias2 = mediaService.createMedia(medias);
            for(Media media: medias)
            {
               // media.setMessage(message);
                str = str + sourceFile + media.getUrl() + "\n";
            };

        }
        message.setCorps(message.getCorps() + "\n Les Pièces-jointes: \n" + str);
        try {
            simpleEmailService.sendEmail(message);
        } catch (Exception e) {
            e.printStackTrace();
        }
        message.setDateEnvoi(LocalDateTime.now());
        Message m = messageService.saveMessage(message);

        if (!medias.isEmpty()) {
            for(Media media: medias)
            {
                media.setMessage(m);
                mediaRepository.saveAndFlush(media);

            }

        }
        return m;

    }



    /*@RequestMapping(value = "/get", method = RequestMethod.GET)
    public Image get(@RequestParam("name") String name) {
        System.out.println(String.format("/GET info: imageName = %s", name));
        String imagePath = "C:\\server\\" + name;
        String imageBase64 = UtilBase64Image.encoder(imagePath);

        if(imageBase64 != null){
            Image image = new Image(name, imageBase64);
            return image;
        }
        return null;
    }*/

    @PutMapping(value = "/messages/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Message updateMessage(@RequestBody Message message, @PathVariable Long id ){
        message.setId(id);
        return messageService.updateMessage(message);
    }

    @DeleteMapping(value = "/messages/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteMedia(@PathVariable Long id) { messageService.deleteMessage(id);
    }
}
