package com.isep.sitewebisepback.controller;


import com.isep.sitewebisepback.model.Media;
import com.isep.sitewebisepback.services.MediaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


import java.util.List;
@RestController
@CrossOrigin(origins = "*")
public class MediaController {
    @Autowired
    private MediaService mediaService;

    /*@GetMapping(value = "/medias", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Media> getAllMedias() {
        return mediaService.getAllMedias();
    }
    @GetMapping(value = "/medias/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Media getMediaById(@PathVariable Long id) {
        return mediaService.getMediaById(id);
    }

    @RequestMapping(value = "/medias",method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Media saveMedia(@RequestBody Media media) {

        return mediaService.saveMedia(media);

    }*/


    @RequestMapping(value = "/medias", method = RequestMethod.POST)
    public List<Media> post(@RequestBody List<Media> medias) {
        // save Image to C:\\server folder
        return mediaService.createMedia(medias);

    }

    /*@RequestMapping(value = "/get", method = RequestMethod.GET)
    public Image get(@RequestParam("name") String name) {
        System.out.println(String.format("/GET info: imageName = %s", name));
        String imagePath = "C:\\server\\" + name;
        String imageBase64 = UtilBase64Image.encoder(imagePath);

        if(imageBase64 != null){
            Image image = new Image(name, imageBase64);
            return image;
        }
        return null;
    }*/

    @PutMapping(value = "/medias/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Media updateMedia(@RequestBody Media media, @PathVariable Long id ){
        media.setId(id);
        return mediaService.updateMedia(media);
    }

    @DeleteMapping(value = "/medias/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteMedia(@PathVariable Long id) { mediaService.deleteMedia(id);
    }
}
